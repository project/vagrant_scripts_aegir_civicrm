#! /usr/bin/env python

"""Common functions for the Aegir provision_civicrm testing scripts
"""

import fabric.api as fabric
import os, sys, glob

def fab_run_provision_civicrm_tests():
        print "===> Running provision_civicrm tests"
        fabric.sudo("su - -s /bin/sh aegir -c 'drush cache-clear drush -y || true'", pty=True)
        fabric.sudo("su - -s /bin/sh aegir -c 'drush @hostmaster provision-civicrm-tests-run -y'", pty=True)

def fab_fetch_provision_civicrm(release_type = 'git', provision_civicrm_version = '6.x-1.x'):
        if release_type == "git":
                print "===> Fetching provision_civicrm - via git"
                fabric.sudo("su - -s /bin/sh aegir -c 'git clone http://git.drupal.org/project/provision_civicrm.git ~/.drush/provision_civicrm'", pty=True)
                fabric.sudo("su - -s /bin/sh aegir -c 'cd ~/.drush/provision_civicrm && git checkout %s'" % (provision_civicrm_version), pty=True)
        else:
                print "===> Fetching provision_civicrm - via package"
                fabric.sudo("su - -s /bin/sh aegir -c 'php /var/aegir/drush/drush.php dl -y --destination=/var/aegir/.drush provision_civicrm-%s'" % (provision_civicrm_version), pty=True)

